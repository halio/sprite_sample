#pragma once
class Rect
{
public:
	Rect();
	~Rect();

	void SetX(int x);
	void SetY(int y);

	int X();
	int Y();
private:
	int m_x;
	int m_y;
};

