#include "stdafx.h"
#include "rect.hpp"

Rect::Rect()
:	m_x(0),
	m_y(0)
{
}

Rect::~Rect()
{
}

void Rect::SetX(int x)
{
	m_x = x;
}

void Rect::SetY(int y)
{
	m_y = y;
}

int Rect::X()
{
	return m_x;
}

int Rect::Y()
{
	return m_y;
}