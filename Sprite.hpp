#pragma once

#include "rect.hpp"
class Sprite
{
public:
	Sprite();
	~Sprite();

	void SetX(int x);
	void SetY(int y);

	int GetX();
	int GetY();

	Rect* GetRect();

private:
	Rect m_rect;
};

