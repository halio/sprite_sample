#include "stdafx.h"
#include <iostream>
#include <vector>

#include "rect.hpp"
#include "Sprite.hpp"

int main()
{
	std::vector<Sprite*> spriteList;

	Sprite *sprite = new Sprite();
	sprite->SetX(29); sprite->SetY(59);

	spriteList.push_back(sprite);

	sprite = new Sprite();
	sprite->SetX(500); sprite->SetY(1000);

	spriteList.push_back(sprite);

	//Iterating through list and printing X and Y values
	for (Sprite *spr : spriteList) {
		Rect *rect = spr->GetRect();

		rect->SetX(2000); rect->SetY(3000);

		std::cout << "RectVal: { X:" << rect->X() << ", Y:" << rect->Y() << " }" << std::endl;
	}

	//Cleaning up
	for (Sprite *spr : spriteList) {
		//check if pointer is pointing to somewhere, equivalent to this will be (ptr === NULL)
		if (spr) {
			delete spr;
		}
	}

	std::cout << "Existing test app" << std::endl;
	return 0;
}

