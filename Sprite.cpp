#include "stdafx.h"
#include "Sprite.hpp"
#include "rect.hpp"

Sprite::Sprite()
{
}


Sprite::~Sprite()
{
}


void Sprite::SetX(int x)
{
	m_rect.SetX(x);

}

void Sprite::SetY(int y)
{
	m_rect.SetY(y);
}

Rect* Sprite::GetRect()
{
	return &m_rect;
}

int Sprite::GetX()
{
	return m_rect.X();
}

int Sprite::GetY()
{
	return m_rect.Y();
}